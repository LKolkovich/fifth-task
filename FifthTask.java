import java.util.Scanner;

public class FifthTask {
    public static boolean IsPalindrom(String str){ // проверяет, является ли слов палиндромом
        for(int i = 0; i < str.length() / 2; i++){
            if(!(str.substring(i, i + 1).equals(str.substring(str.length() - i - 1, str.length() - i))))
                return false;
        }
        return true;
    }
    public static void FindingPalindrom(String str){ // Для каждого слова в строке выполняется IsPalindrom
        String[] words = str.split(" ");
        for(String word : words){
            if(IsPalindrom(word)){
                System.out.println(word);
            }
        }
    }
    public static boolean IsLatin(String str){ // проверяет есть ли в слове какие-то символы, кроме латинских букв
        int a = 'a';
        int z = 'z';
        int A = 'A';
        int Z = 'Z';
        for(char sim : str.toCharArray()){
            if(!((sim >= a && sim <= z) || (sim >= A && sim <= Z))){
                return false;
            }
        }
        return true;
    }

    public static int LatinCount(String str){ // Для каждого слова вызывает IsLatin и считает количество слов, состоящих из латинских букв
        String[] words = str.split(" ");
        int count = 0;
        for(String current : words){
            if(IsLatin(current))
                count++;
        }
        return count;
    }
    public static int UnicSims(String str){ // считает колиество уникальных символов в слове
        String check = "";
        int count = 0;
        boolean flag;
        for(int i = 0; i < str.length(); i++){
            flag = true;
            for(int j = 0; j < check.length(); j++){
                if(str.substring(i, i + 1).equals(check.substring(j, j + 1))){
                    flag = false;
                    break;
                }
            }
            if(flag) {
                check += str.substring(i, i + 1);
                count += 1;
            }
        }
        return count;
    }
    public static String FindWordWithLessUnicSims(String str){ // для каждого слова вызывает UnicSims и ищет слово с минимальным количеством уникальных символов
        String[] words = str.split(" ");
        int countOfSim;
        int minCountOfSim = 10000;
        String minWord = "";
        for(String word: words){
            countOfSim = UnicSims(word);
            if(countOfSim < minCountOfSim){
                minCountOfSim = countOfSim;
                minWord = word;
            }
        }
        return minWord;
    }

    public static String OopReplace(String s){ // заменяет каждое второе вхождение object-oriented programming на OOP
        int len = s.length();
        String stringForReplace = "OOP";
        String replaceMask = "object-oriented programming";
        StringBuilder str = new StringBuilder(s);
        int start = 0;
        int end = replaceMask.length();
        int numOfReplace = 1;

        while(end <= len){
            String checkEqu = str.substring(start,end); // срез строки, длина которого равна длине строки, которую надо заменить
            checkEqu = checkEqu.toLowerCase(); // замена букв на строчные
            if(checkEqu.equals(replaceMask)){
                if(numOfReplace % 2 == 0) { // если это четный номер вхождения подстроки в строку, то заменяем её
                    str.replace(start, end , stringForReplace);
                    len = len - replaceMask.length() + 3; // сооответсвующим образом изменяеям len, start и end
                    start = start - replaceMask.length();
                    end = end - replaceMask.length();
                }
                numOfReplace++;
            }
            start++;
            end++;
        }
        return str.toString();
    }

    public static void main(String[] args) {
        Scanner myScan = new Scanner(System.in);
        String s = myScan.nextLine();

        String str = OopReplace(s); // строка с заменами на OOP

        System.out.println(str);

        System.out.println(FindWordWithLessUnicSims(str)); // выводит слово с минимальным количеством уникальных символов

        System.out.println(LatinCount(str)); // выводит количство слов, содержащих только буквы латинского алфавита

        FindingPalindrom(str); // вызов функции, которая ищет палиндромы
    }
}
